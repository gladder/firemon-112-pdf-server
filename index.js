const express = require("express");
const puppeteer = require("puppeteer");
const app = express();
const Buffer = require('buffer').Buffer;

app.get("/pdf", async (req, res) => {
    
    try {
        const base64_url = req.query.target;
        const url = Buffer.from(base64_url, 'base64').toString('UTF-8');
        console.log("convert " + url + " to pdf");
        const browser = await puppeteer.launch({
            headless: true,
            args: [
                "--disable-gpu",
                "--disable-dev-shm-usage",
                "--disable-setuid-sandbox",
                "--no-sandbox",
            ]
        });
    
        const webPage = await browser.newPage();
        
        //webPage.on('console', async msg => console[msg._type](
        //    ...await Promise.all(msg.args().map(arg => arg.jsonValue()))
        //  ));

        await webPage.goto(url, {
            waitUntil: "networkidle0"
        });
       
        const watchdog = webPage.waitForFunction('window.alert_gui_rendered === true', {timeout: 15000, polling: 1000});
        await watchdog;

        const pdf = await webPage.pdf({
            printBackground: true,
            format: "A4",
            margin: {
                top: "20px",
                bottom: "40px",
                left: "20px",
                right: "20px"
            }
        });
    
        await browser.close();
    
        res.contentType("application/pdf");
        res.send(pdf);

    } catch(e) {
        console.log(e);
        res.send(500);
    }
    
    
})

app.listen(3000, () => {
    console.log("Server started");
});